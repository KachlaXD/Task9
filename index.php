<?php
$products = [
    ['id' => 1, 'name' => 'RTX 3060 super 12Gb', 'price' => 120],
    ['id' => 2, 'name' => 'RTX 3080Ti 12Gb', 'price' => 130],
    ['id' => 3, 'name' => 'RTX 4070Ti 16Gb', 'price' => 200],
];

$cart = [];


if (isset($_POST['add_to_cart'])) {
    $product_id = $_POST['product_id'];
    $cart[] = $products[$product_id - 1];
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>digmous</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <header>
        <h1>Dighmous shop</h1>
    </header>

    <div id="product-list">
        <?php foreach ($products as $product): ?>
            <div class="product">
                <h3><?php echo $product['name']; ?></h3>
                <p>Price: $<?php echo $product['price']; ?></p>
                <form method="post">
                    <div><img src="https://s.alicdn.com/@sc04/kf/H5bd96d51e0594eb890be815b8cec64a9i.jpg_200x200.jpg" alt=""></div>
                    <input type="hidden" name="product_id" value="<?php echo $product['id']; ?>">
                    <input type="submit" name="add_to_cart" value="Add to Cart">
                </form>
            </div>
        <?php endforeach; ?>
    </div>

    <div>
        <h2>Shopping Cart</h2>
        <?php if (!empty($cart)): ?>
            <ul>
                <?php foreach ($cart as $item): ?>
                    <li><?php echo $item['name']; ?> - $<?php echo $item['price']; ?></li>
                <?php endforeach; ?>
            </ul>
        <?php else: ?>
            <p>Your cart is empty.</p>
        <?php endif; ?>
    </div>

    <footer>
        <div></div>
    </footer>
</body>
</html>